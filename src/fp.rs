pub fn three_point_ohs_four() {
	println!("Floating point representation innaccuracies");
	println!("See https://0.30000000000000004.com/");
	println!("printf 1.2+1.7 to 54 decimal places: {:.54}", 1.2+1.7);
	let  a = 1.2;
	let b = 1.7;
	println!("a + b: {}", a + b);
	let mut total = 0.0;
	for _i in 1..79 {
		total = total + a + b;
	}
	println!("total: {}", total);
	println!("total + 0.11: {}", total + 0.11);
	println!("print total to 54 places: {:.54}", total);
	println!("print total * 9 to 54 places: {:.54}", total*9.0);
}

pub fn muller(y: f64, z: f64) -> f64{
	return 108.0 - (815.0 - 1500.0/z)/y;
}
