pub mod var;
mod fp;

fn main() {
    println!("Hello, world!");
    var::vars();
    fp::three_point_ohs_four();
    let mut y = 4.25;
    let mut z = 4.0;
    for index  in 1..30 {
	let x = fp::muller(y, z);
        println!("{}: {:.54}", index, x);
        z = y;
        y = x;
    }
    // TODO pull the rug
}
