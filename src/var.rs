pub fn vars() {
    let x = 5;
    println!("x has the value {}", x);
    let mut s = String::from("hello");
    s.push_str(", world!");
    println!("{}", s);
}

